module GPA2

using FoldingTrees
using Statistics: mean
using HDF5
using AbstractTrees
using TreeHelper: savetree, getpath

using Dates

using ProgressMeter

function gpa(t,process,weight,x₀,C;processParams=Dict(),weightParams=Dict(),savefile=nothing,ntasks=1)
# perform the GPA algorithm
    Z = Float64[]
    nodeid = 0
    root = Node(Dict{Symbol,Any}(:x => nothing, :path => nothing, :id => nodeid, :weight => nothing))
    nodeid += 1
    alive = typeof(root)[]
    for xᵢ in x₀
        newnode = Node(Dict{Symbol,Any}(:x => xᵢ, :path => nothing, :id => nodeid, :weight => nothing),root)
        nodeid += 1
        push!(alive,newnode)
    end
        
    # initial evolution
    # extend the genealogical tree
    xout = asyncmap(node->process([t[1],t[2]],node.data[:x]; processParams...),alive;ntasks=ntasks)
    newalive = eltype(alive)[]
    for (node,(xᵢ, path)) in zip(alive,xout)
        newnode = Node(Dict{Symbol,Any}(:x => xᵢ, :path => path, :id => nodeid, :weight => nothing),
                            node)
        nodeid += 1
        push!(newalive,newnode)
    end
    alive = newalive
    if !isnothing(savefile)
        h5open(savefile, "w") do file
                write(file, "Z", Z)
                write(file, "C", C)
                write(file, "weight", string(weightParams))
                write(file, "process", string(processParams))
                savetree(file,root)
            end
    end

    @showprogress for k=1:(length(t)-2)
        # select
        W = weight(t[k+1] - t[k], [map(n->n.data[:x],getpath(node)) for node in alive], [map(n->n.data[:path],getpath(node)) for node in alive], C;weightParams...)
        append!(Z, [mean(W)])
        W = W/Z[end]
        N₊ = floor.(Integer, W .+ rand(length(W)))
        for i=1:length(alive)
            push!(alive[i].data, :weight => W[i], :children => N₊[i])
        end
    
        newalive = eltype(alive)[]
        xin = vcat(fill.(alive, N₊)...)
        xout = asyncmap(node->process([t[k+1],t[k+2]],node.data[:x]; processParams...),xin;ntasks=ntasks)
        for (node,(xᵢ, path)) in zip(xin,xout)
#                xᵢ, path = process([t[k+1],t[k+2]], alive[i].data[:x]; processParams...)
                newnode = Node(Dict{Symbol,Any}(:x => xᵢ, :path => path, :id => nodeid, :weight => nothing)
                            ,node)
                nodeid += 1
                push!(newalive, newnode)
        end
        alive = newalive
        
        if !isnothing(savefile)
            h5open(savefile, "w") do file
                write(file, "Z", Z)
                write(file, "C", C)
                savetree(file,root)
                write(file, "weight", string(weightParams))
                write(file, "process", string(processParams))
            end
        end
    end

    return root, alive, Z
end

end
