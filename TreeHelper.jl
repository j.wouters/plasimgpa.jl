module TreeHelper

using FoldingTrees
using AbstractTrees
using HDF5

    #https://www.geeksforgeeks.org/serialize-deserialize-n-ary-tree/
    # see

    children(nodes::Vector{<:Node}) = vcat([node.children for node in unique(nodes)]...)

    parents(nodes::Array{Node,1}) = unique([node.parent for node  in nodes])
    haschildren(node::Node) = length(node.children) > 0 ? true : false

    function getroot(node::Node)
        while !isroot(node)
            node = node.parent
        end
        node
    end
    
    function generation(roots::Vector{<:Node},steps::Int64)::Vector{<:Node}
        if steps>0
            return generation(children(roots),steps-1)
        else
            return roots
        end
    end

    generation(node::Node,steps::Int64) = generation([node],steps)

    function depth(node::Node)
        depth=0
        while !isroot(node)
            depth += 1
            node = node.parent
        end
        depth
    end

    function savetree(file,root)
        ids = map(node -> node.data[:id], PostOrderDFS(root))
        ###
        f(x) = isnothing(x) ? "" : x
        g(x) = isnothing(x) ? NaN : x
        xᵢ = map(node -> f(node.data[:x]), PostOrderDFS(root))
        paths = map(node -> f(node.data[:path]), PostOrderDFS(root))
        weights = map(node -> g(node.data[:weight]), PostOrderDFS(root))
        ###
        children = hcat(vcat(map(node -> map(child -> [node.data[:id], child.data[:id]], node.children), PostOrderDFS(root))...)...)
        write(file,"ids",ids)
        write(file,"children",children)
        ###
        write(file,"states",xᵢ)
        write(file,"paths",paths)
        write(file,"weights",weights)
        ###
    end

    function loadtree(h5file)
        children = h5read(h5file,"children");
        ids = h5read(h5file,"ids");
        ###
        paths = h5read(h5file,"paths");
        states = h5read(h5file,"states");
        weights = h5read(h5file,"weights");
        ###

        if length(findall(ids .== 0)) > 1
            error()
        end
        root_index = findall(ids .== 0)[1]
        ###
        root = Node(Dict{Symbol,Any}(:x => states[root_index],
                                :path => paths[root_index],
                                :weight => weights[root_index],
                                :id => ids[root_index]))
        ###
        alive = [root]
        while true
            newalive = eltype(alive)[]
            for node in alive
                child_ids = children[2,findall(children[1,:] .== node.data[:id])]
                for child_id in child_ids
                    if length(findall(ids .== child_id)) > 1
                        error()
                    end
                    child_index = findall(ids .== child_id)[1]
                    ###
                    newnode = Node(Dict{Symbol,Any}(:x => states[child_index], 
                                                    :path => paths[child_index],
                                                    :weight => weights[child_index],
                                                    :id => ids[child_index]),node)
                    ###
                    push!(newalive,newnode)
                end
            end
            isempty(newalive) ? break : alive = newalive
        end
        
        return root, alive
    end


    function getpath(node)
        path = Array{typeof(node),1}()
        while !isroot(node)
            if !isnothing(node.data[:path]) && isfile(node.data[:path])
                 prepend!(path,[node])
            end
            node = node.parent
        end
        return path
    end
end
