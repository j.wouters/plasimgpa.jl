module Plasim

using Printf
using PyCall
using SHA
using Dates
using Random
using FortranFiles

# Example run
# Plasim.run([Date(0,1,1),Date(0,1,10)],nothing,Plasim.namelists,
#       res=42,levels=10,outputDir="/home/jeroen/tmp/test/",numCores=2)

namelists = Dict(
   "oceanmod" => Dict("nocean" => 0, "nlsg" => 0),
   "planet" => Dict("eccen" => 0.0167, "mvelp" => 102.7, "obliq" => 23.44, "gsol0" => 1367.0, "nfixorb" => 0),
   "plasim" => Dict(
       "noutput" => 1, "ngui" => 0, "n_start_year" => 1, "n_days_per_year" => 365, "n_run_years" => 0,
       "n_run_months" => 0, "n_run_days" => 0, "kick" => 1, "mpstep" => 20,
       "naqua" => 0, "ndiag" => 0, "nguidbg" => 0, "nqspec" => 1, "nveg" => 0, "nwpd" => 1,
       "nprint" => 0, "nsync" => 0, "syncstr" => 0.0),
   "radmod" => Dict("co2" => 360.0),
   "rainmod" => Dict("nclouds" => 1, "nstorain" => 0),
   "icemod" => Dict(),
   "fluxmod" => Dict(),
   "landmod" => Dict(), 
   "miscmod" => Dict(), 
   "surfmod" => Dict(), 
   "seamod" => Dict(),
   "vegmod" => Dict()
   )

# standard set of output variables
allvars = ["sg", "pl", "ta", "hus", "d", "zeta", "mld", "ts", "mrso", "snd", "pr", "prl", "prc", "prsn", "hfss", "hfls", "clw", "u3", "mrro", "cl", "clt", "tas", "tsa", "tsod", "lsm", "z0", "as", "rss", "rls", "rst", "rlut", "tauu", "tauv", "evap", "tso", "tasmax", "tasmin", "rsut", "ssru", "stru", "tso2", "tso3", "tso4", "sic", "sit", "vegf", "snm", "sndc", "dwmax", "prw", "glac", "zg"]

# run for a δt days
function run(δt::Int64,x₀::Union{Nothing,String}; t₀::Int64=0, kwargs...)
    run(Day(δt),x₀;t₀=Date(t₀),kwargs...)
end
function run(δt::Day,x₀::Union{Nothing,String}; t₀=Date(0,1,1), kwargs...)
    run([t₀, t₀ + δt],x₀;kwargs...)
end

# run saving restarts at intervals given by Int Array t
function run(t::Array{Int64,1},x₀::Union{Nothing,String}; t₀=Date(0,1,1), kwargs...)
    run(t₀ .+ Day.(t),x₀;kwargs...)
end

# runs from multiple initial conditions
function run(t,x₀::Array{String,1};
                    numTasks::Int=1,ExperimentName::String="PLASIM",kwargs...)        
    asyncmap(x->run(t,x;ExperimentName = ExperimentName*"_"*string(now())*"_"*randstring(),kwargs...),x₀;ntasks=numTasks)
end

# run with configuration specified as Dict of Dicts
function run(t::Array{Date,1},x₀::Union{Nothing, String},
					conf::Dict{String, Dict}=namelists;                    
                    runDir::String=joinpath(homedir(),"tmp/jl_"*randstring()),kwargs...)
    if !isdir(runDir)
        mkpath(runDir)
    end
    f90nml = pyimport("f90nml")
    for k in keys(conf)
         f90nml.write(Dict(k*"_nl"=>conf[k]),joinpath(runDir,k*"_namelist"),force=true)
    end
    run(t,x₀,runDir;runDir=runDir,kwargs...)
end

function run(t::Array{Date,1},x₀::Union{Nothing, String},
                    confDir::String;
                    res::Int64,levels::Int64,outputDir::String, # required args
                    numCores::Int=1,
                    PlaSimExec::String="plasim_t"*string(res)*"_l"*string(levels)*"_p"*string(numCores),
                    runDir::String=joinpath(homedir(),"tmp/jl_"*randstring()),
                    netcdfOut::Bool=true,
                    startIndex::Int64=1,
                    ExperimentName::String="PLASIM_"*string(now())*"_"*randstring(),
                    outvars::Vector{String}=allvars,
                    datDir::String=joinpath(homedir(),".local/share/plasim/dat/T$res"))

# The runDir needs to be accessible from all nodes when run in parallel on several
# machines.

    if !isdir(outputDir)
        mkpath(outputDir)
    end
    if !isdir(runDir)
        mkpath(runDir)
    end
    
    # copy over configuration files
	for file in readdir(confDir)
	    if endswith(file,"_namelist")
	        (confDir != runDir) && cp(joinpath(confDir,file),joinpath(runDir,file),force=true)
	        cp(joinpath(confDir,file),joinpath(outputDir,file),force=true)
	    end
	end
    # copy over data files
    if datDir != runDir
		for file in readdir(datDir)
		    if endswith(file,".sra")
		        cp(joinpath(datDir,file),joinpath(runDir,file),force=true)
		    end
		end
	end

    # remove old restart files
    abortFile = joinpath(runDir, "Abort_Message")
    if isfile(abortFile)
        rm(abortFile)
    end
    for file in readdir(runDir)
        if startswith(file,"plasim_restart")
            rm(joinpath(runDir,file))
        end
    end

    # link to restart file(s) if specified
    if !isnothing(x₀)
        if isfile(x₀)
            symlink(abspath(x₀), joinpath(runDir,"plasim_restart"))
            open(x₀) do f
               open(joinpath(outputDir,"$(ExperimentName)_restart_hash"), "w") do io
                   write(io, bytes2hex(sha2_256(f)))
                end;
            end
        else
            error("Error: specified restart file does not exist.")
        end
    end

    f90nml = pyimport("f90nml")
    for i in 1:(length(t)-1)
        run_ind = startIndex + i - 1
        
        plasim_nl = f90nml.read(joinpath(runDir,"plasim_namelist"))
        plasim_nl["plasim_nl"]["n_run_days"] = Dates.days(t[i+1] - t[i])
        f90nml.write(plasim_nl,joinpath(runDir,"plasim_namelist"),force=true)
        
        # run PlaSim
        run(Cmd(`mpiexec -np $numCores -wdir $runDir $PlaSimExec`))

        if isfile(joinpath(runDir,"Abort_Message"))
            sys.exit("Error: PlaSim aborted")
        end

        # copy output, log and final condition files
        DataName = @sprintf("%s.%03d", ExperimentName, run_ind)
        DiagName = @sprintf("%s_DIAG.%03d", ExperimentName, run_ind)
        RestartName = @sprintf("%s_REST.%03d", ExperimentName, run_ind)

        if isfile(joinpath(runDir,"plasim_output"))
            mv(joinpath(runDir,"plasim_output"), joinpath(outputDir, DataName),force=true)
            mv(joinpath(runDir,"plasim_diag"),joinpath(outputDir, DiagName),force=true)
        end
        if length(t) > 2
            cp(joinpath(runDir,"plasim_status"), joinpath(runDir,"plasim_restart"),force=true)
        end
        mv(joinpath(runDir,"plasim_status"), joinpath(outputDir, RestartName),force=true)
    end
    
    #burnConf = "code="*prod(map(x->x*",",outvars[1:end-1]))*outvars[end]
    burnConf = "code="*join(outvars,",")
    if length(t) > 2
        burnConf *= "\nmulti=$(length(t)-1)"
    end
    if netcdfOut
        burnConf *= "\nnetcdf=1"
    end
    burnConf *= "\nmean=0"
    
    if netcdfOut
        let DataName = @sprintf("%s.%03d", ExperimentName, startIndex)
            open(Cmd(`burn7 $DataName $ExperimentName.nc`,dir=outputDir),"w") do f
                write(f,burnConf)
            end
        end
    end;

#    if netcdfOut
#        let DataName = @sprintf("%s.%03d", ExperimentName, startIndex)
#            run(Cmd(`srv2nc $DataName $ExperimentName.nc`,dir=outputDir))
#        end
#    end  
 
#    rm(runDir,force=true,recursive=true)
    rm(joinpath(runDir,"ice_output"),force=true)
    rm(joinpath(runDir,"ocean_output"),force=true)
    rm(joinpath(runDir,"plasim_restart"),force=true)
    for file in readdir(runDir)
        if endswith(file,".sra") || endswith(file,"_namelist")
            rm(joinpath(runDir,file),force=true)
        end
    end
    if isempty(readdir(runDir))
        rm(runDir,force=true)
    end
    
    if !netcdfOut
        return joinpath(outputDir, @sprintf("%s_REST.%03d", ExperimentName, length(t)-1)), nothing
    else
        return joinpath(outputDir, @sprintf("%s_REST.%03d", ExperimentName, length(t)-1)),
                joinpath(outputDir,"$ExperimentName.nc")
    end
end

end # module
