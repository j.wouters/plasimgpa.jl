using Plasim
using Dates
using ProgressMeter
using HDF5

res = (@isdefined _res) ? _res : 42
levels = (@isdefined _levels) ? _levels : 10
npro = (@isdefined _npro) ? _npro : 16
nyears = (@isdefined _nyears) ? _nyears : 1000

experimentName = "control-t"*string(res)*"-l"*string(levels)*"-p"*string(npro)
outputDir = joinpath("data",experimentName)
#confDir = joinpath(homedir(),".local/share/plasim/namelists/")
confDir = "conf-control"

if !((@isdefined restart0)||(@isdefined year0))
    #initial run (January, February)
    restart0, ncfile0 = Plasim.plasimrun([Date(1,1,1),Date(1,3,1)],nothing,
                        confDir=confDir,res=res,levels=levels,outputDir=outputDir,numCores=npro)
end
year0=1

vars =  ["pr", "prw", "prl", "prc", "prsn", "ta", "ua", "va", "hus", "ps", "wap", "wa", "zeta", "ts", "stf", "psi", "psl", "d", "zg", "hur", "clw", "cl", "clt"]

keep_mam = true
keep_jja = true
keep_son = true
keep_djf = true


restarts = Array{String}(undef,nyears,4)
ncfiles = Array{String}(undef,nyears,4)

@showprogress for year in year0:nyears
    #MAM
    experiment_mam = experimentName*"-y"*string(year)*"-mam"
    if !isfile(joinpath(outputDir,experiment_mam*"_REST.001"))
        restart_mam, ncfile_mam = Plasim.plasimrun([Date(year,3,1),Date(year,6,1)],
                                        restart0,confDir=confDir,res=res,levels=levels,
                                        outputDir=outputDir,numCores=npro,ExperimentName=experimentName*"-y"*string(year)*"-mam",
                                        outvars=vars)
    else
        restart_mam = joinpath(outputDir,experiment_mam*"_REST.001")
        ncfile_mam = joinpath(outputDir,experiment_mam*".nc")
    end
    restarts[year,1] = restart_mam
    ncfiles[year,1] = ncfile_mam
    
    rm(joinpath(outputDir,experiment_mam*".001"),force=true)
    if !keep_mam
        rm(joinpath(outputDir,experiment_mam*".nc",force=true))
    end
    
    #JJA
    experiment_jja = experimentName*"-y"*string(year)*"-jja"
    if !isfile(joinpath(outputDir,experiment_jja*"_REST.001"))
        restart_jja, ncfile_jja = Plasim.plasimrun([Date(year,6,1),Date(year,9,1)],
                                        restart_mam,confDir=confDir,res=res,levels=levels,
                                        outputDir=outputDir,numCores=npro,ExperimentName=experimentName*"-y"*string(year)*"-jja",
                                        outvars=vars)
    else
        restart_jja = joinpath(outputDir,experiment_jja*"_REST.001")
        ncfile_jja = joinpath(outputDir,experiment_jja*".nc")
    end
    restarts[year,2] = restart_jja
    ncfiles[year,2] = ncfile_jja
    
    rm(joinpath(outputDir,experiment_jja*".001"),force=true)
    if !keep_jja
        rm(joinpath(outputDir,experiment_jja*".nc",force=true))
    end
    
    #SON
    experiment_son = experimentName*"-y"*string(year)*"-son"
    if !isfile(joinpath(outputDir,experiment_son*"_REST.001"))
        restart_son, ncfile_son = Plasim.plasimrun([Date(year,9,1),Date(year,12,1)],
                                        restart_jja,confDir=confDir,res=res,levels=levels,
                                        outputDir=outputDir,numCores=npro,ExperimentName=experimentName*"-y"*string(year)*"-son",
                                        outvars=vars)
    else
        restart_son = joinpath(outputDir,experiment_son*"_REST.001")
        ncfile_son = joinpath(outputDir,experiment_son*".nc")
    end
    restarts[year,3] = restart_son
    ncfiles[year,3] = ncfile_son
    
    rm(joinpath(outputDir,experiment_son*".001"),force=true)
    if !keep_son
        rm(joinpath(outputDir,experiment_son*".nc",force=true))
    end
    
    #DJF
    experiment_djf = experimentName*"-y"*string(year)*"-djf"
    if !isfile(joinpath(outputDir,experiment_djf*"_REST.001"))
        restart_djf, ncfile_djf = Plasim.plasimrun([Date(year,12,1),Date(year+1,3,1)],
                                        restart_son,confDir=confDir,res=res,levels=levels,
                                        outputDir=outputDir,numCores=npro,ExperimentName=experimentName*"-y"*string(year)*"-djf",
                                        outvars=vars)
    else
        restart_djf = joinpath(outputDir,experiment_djf*"_REST.001")
        ncfile_djf = joinpath(outputDir,experiment_djf*".nc")
    end
    restarts[year,4] = restart_djf
    ncfiles[year,4] = ncfile_djf
    
    rm(joinpath(outputDir,experiment_djf*".001"),force=true)
    if !keep_djf
        rm(joinpath(outputDir,experiment_djf*".nc",force=true))
    end
    
    global restart0 = restart_djf
end
