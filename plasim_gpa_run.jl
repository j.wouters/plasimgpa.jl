using GPA2
using Plasim
using PlasimWeights

using Random
using Distributions
using Dates
using HDF5
using NCDatasets

gpa_experiment = "PS-GPA_"*Dates.format(Dates.now(),"dd-u-yyyy-HH:MM")*"-"*randstring(4)
param_file = "eur_large_avg_params.h5"

if @isdefined _M
    M = _M
else
    M = 250
end

control_dir = "data/control-t42-l10-p16/"
xskip = 10
x₀ = [joinpath(control_dir,"control-t42-l10-p16-y$(string(i))-son_REST.001") for i in (xskip+1):(xskip+M)];
if sum((!isfile).(x₀)) > 0
    error("Restart file not found")
end

t₀=0
if @isdefined _t₁
    t₁ = _t₁
else
    t₁ = 90
end

if @isdefined _δt
    δt = _δt
else
    δt = 5
end

t=t₀:δt:t₁


#weightParams = Dict(:regions => [((0.,30.),(50.,70.)), ((340.,30.),(360.,70.))], :obs => :pr)
weightParams =  
let regions = vcat(mapslices(a->tuple(mapslices(x->tuple(x...),a,dims=1)...),
                h5read(param_file,"regions"),dims=(1,2))...),
    obs = h5read(param_file,"obs")
   Dict(:regions => regions, :obs => obs)
end

if @isdefined _C
    C = _C
else
    k = 2 
    if h5open(param_file, "r") do fid
        g = fid["/"]
        haskey(g,"alpha") && haskey(g,"theta")
        end
    
        α = h5read(param_file,"alpha")
        θ = h5read(param_file,"theta")
        C = k/(θ*(k + sqrt(α)))
    else
        control_h5files = filter(endswith("-djf.h5"),readdir(control_dir))
        prs = Array{Float32}(undef,size(control_h5files,1),90)
        for i = 1:size(control_h5files,1)
            pr = h5read(joinpath(control_dir,control_h5files[i]),"pr_avg")
            prs[i,:] = pr
        end
        cumprs = mapslices(v->integrate(Dates.value.((1:90).*Second(Day(1))),v),prs,dims=2);
        # shift the mean by k*σ, assuming a Gamma distribution
        F = fit(Gamma,cumprs)
        C = k/(F.θ*(k + sqrt(F.α)))
    end
end

if @isdefined _ntasks
    ntasks = _ntasks
else
    ntasks = 1
end
outdir = "data/$gpa_experiment"

processParams = Dict(:levels => 10, :res => 42, :confDir => "conf", :outputDir => outdir,:numCores => 16)

savefile = joinpath(outdir,"$gpa_experiment.h5")
println("M=",M," t=",t, " C=",C)
println("savefile = ",savefile)
println("params = ",processParams)
println("ntasks = ",ntasks)
println("weight conf = ",weightParams)
root,alive,Z = GPA2.gpa(t,Plasim.plasimrun,PlasimWeights.weight2,x₀,C;
                        savefile=savefile,processParams=processParams,weightParams=weightParams,
                        ntasks=ntasks)
