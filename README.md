# PlasimGPA.jl

Julia packages and scripts to perform Rare Event Simulation based on Genealogical Particle Analysis with [PlaSim](https://github.com/HartmutBorth/PLASIM).
