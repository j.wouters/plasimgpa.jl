module PlasimWeights

using GeoAvg

function weight(t,states,paths,C;regions,obs)
    weights = []
    for i=1:length(paths)
        avg = GeoAvg.geo_avg_time_int(paths[i],obs,regions)
        push!(weights,exp(C*avg))
    end
    return weights
end

function weight2(t,states,paths,C;regions,obs)
    weights = []
    for i=1:length(paths)
        if length(paths[i]) == 1
            avg = GeoAvg.geo_avg_time_int(paths[i][1],obs,regions)
            push!(weights,exp(C*avg))
        else
            new_avg = GeoAvg.geo_avg_time_int(paths[i],obs,regions)
            old_avg = GeoAvg.geo_avg_time_int(paths[i][1:end-1],obs,regions)
            if length(C) >= length(paths[i])
                push!(weights,exp(C[length(paths[i])]*new_avg - C[length(paths[i])-1]*old_avg))
            elseif length(C) == 1
                push!(weights,exp(C*new_avg - C*old_avg))
            else
                error("Incorrect length of C")
            end
        end
    end
    return weights
end

end
