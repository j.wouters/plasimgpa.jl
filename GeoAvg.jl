module GeoAvg

using Trapz
using Statistics
using NCDatasets
using Dates

function times(ds::NCDataset;timelims=nothing)
    t = ds["time"]
    if isnothing(timelims)
        return t
    else
        return t[timelims[1] .<= t .<= timelims[2]]
    end
end

function times(ncfile::String;timelims=nothing)
    ds = Dataset(ncfile)
    times(ds;timelims=timelims)
end

# spatial averaging

function geo_avg(ds::NCDataset,obs::String,regions::Array{Tuple{Tuple{Float64,Float64},Tuple{Float64,Float64}},1};timelims=nothing)
#    ilon = findfirst(isequal("lon"),dimnames(ds[obs]))
#    ilat = findfirst(isequal("lat"),dimnames(ds[obs]))
#    itime = findfirst(isequal("time"),dimnames(ds[obs]))

    lon = coord(ds[obs],"longitude")
    lat = coord(ds[obs],"latitude")
    time = coord(ds[obs],"time")

    if isnothing(timelims)
        jtime = trues(length(time))
    else
        jtime = timelims[1] .<= time .<= timelims[2]
    end   
    
    # load one timestep at a time to prevent loading data for all timesteps into memory
    avgs = zeros(eltype(ds[obs]),sum(jtime))
    for jt in findall(jtime)
        denom = 0
        for region in regions
            ds_reg = ds[obs][(region[1][1] .< lon .< region[2][1]),(region[1][2] .< lat .< region[2][2]),jt]
            avgs[jt] += sum(ds_reg)
            denom += prod(size(ds_reg))
        end
        avgs[jt] /= denom
    end
        
    return avgs
end

#geo_avg(ncfile::String, obs::Symbol,regions::Array{Tuple{Tuple{Float64,Float64},Tuple{Float64,Float64}},1}) = geo_avg(NCDstack(ncfile)[obs],regions)
geo_avg(ncfile::String, obs::String,
        regions::Array{Tuple{Tuple{Float64,Float64},Tuple{Float64,Float64}},1};
        timelims=nothing) = geo_avg(Dataset(ncfile),obs,regions;timelims=timelims)
geo_avg(ncfiles::Array{String,1},obs::String,
        regions::Array{Tuple{Tuple{Float64,Float64},Tuple{Float64,Float64}},1};
        timelims=nothing) = 
        vcat([geo_avg(ncfile,obs,regions;timelims=timelims) for ncfile in ncfiles]...)

function geo_avg_time_int(ds::NCDataset,obs::String,regions::Array{Tuple{Tuple{Float64,Float64},Tuple{Float64,Float64}},1};timelims=nothing)
            t = times(ds;timelims=timelims)
            t = Dates.value.(Second.(t-t[1]))
            Trapz.trapz(t,geo_avg(ds,obs,regions;timelims=timelims))
end

geo_avg_time_int(ds::String,obs::String,regions::Array{Tuple{Tuple{Float64,Float64},Tuple{Float64,Float64}},1};timelims=nothing) =
        geo_avg_time_int(NCDataset(ds),obs,regions;timelims=timelims)

geo_avg_time_int(dss::Array{String,1},obs::String,regions::Array{Tuple{Tuple{Float64,Float64},Tuple{Float64,Float64}},1};timelims=nothing) =
        sum([geo_avg_time_int(NCDataset(ds),obs,regions;timelims=timelims) for ds in dss])

               
end
